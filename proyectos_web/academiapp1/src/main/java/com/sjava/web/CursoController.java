package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class CursoController {
    private static List<Curso> listaCurso = new ArrayList<Curso>();
    private static int c_contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Curso> getAll(){
        return listaCurso;
    }

    //getId devuelve un registro
    public static Curso getId(int id){
        for (Curso p : listaCurso) {
            if (p.getId()==id){
                return p;
            }
        }
        return null;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Curso al) {
        if (al.getId() == 0){
            c_contador++;
            al.setId(c_contador);
            listaCurso.add(al);
        } else {
            for (Curso ali : listaCurso) {
                if (ali.getId()==al.getId()) {
                   
                    ali.setNombre(al.getNombre());
                    ali.setDuracion(al.getDuracion());
                    ali.setIdprofesor(al.getIdprofesor());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de alumnos
    public static int size() {
        return listaCurso.size();
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        Curso  borrar=null;
        for (Curso  p : listaCurso) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaCurso.remove(borrar);
        }
    }

}