package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class ProfesorController {
    private static List<Profesor> listaProfesor = new ArrayList<Profesor>();
    private static int p_contador = 0;

    static {

        // Profesor a = new Profesor(99,"ricard hernández", "algo@algo.com", "999333");
        // listaProfesor.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Profesor> getAll(){
        return listaProfesor;
    }

    //getId devuelve un registro
    public static Profesor getId(int id){
        for (Profesor p : listaProfesor) {
            if (p.getId()==id){
                return p;
            }
        }
        return null;
    }
   
    //save guarda un Profesor
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Profesor al) {
        if (al.getId() == 0){
        p_contador++;
            al.setId(p_contador);
            listaProfesor.add(al);
        } else {
            for (Profesor ali : listaProfesor) {
                if (ali.getId()==al.getId()) {
                   
                    ali.setNombre(al.getNombre());
                    ali.setEmail(al.getEmail());
                    ali.setTelefono(al.getTelefono());
                    ali.setEspecialidad(al.getEspecialidad());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de Profesors
    public static int size() {
        return listaProfesor.size();
    }


    // removeId elimina Profesor por id
    public static void removeId(int id){
        Profesor borrar=null;
        for (Profesor p : listaProfesor) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaProfesor.remove(borrar);
        }
    }

}