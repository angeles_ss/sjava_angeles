package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */

public class ProfesorController {
  
    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "profesores";
    private static final String KEY = "idprofesores";
    /*private static final String nombre;
    private static final String email;
    private static final String telefono;
    private static final String especialidad;*/


    // getAll devuelve todos los registros de la tabla
    public static List<Profesor> getAll(){
        
        List<Profesor> listaProfesores = new ArrayList<Profesor>();
		String sql = String.format("select %s,nombre,email,telefono,especialidad from %s", KEY, TABLE);
		//String sql = "select id,nombre,password from "+TABLE;
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Profesor u = new Profesor(
                    (Integer) rs.getObject(1),
                    (String) rs.getObject(2),
                    (String) rs.getObject(3),
                    (String) rs.getObject(4),
                    (String) rs.getObject(5));
				listaProfesores.add(u);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaProfesores;

    }

    //getId devuelve un registro
    public static Profesor getId(int id){
        Profesor u = null;
        String sql = String.format("select %s,nombre,email,telefono,especialidad from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Profesor(
                (Integer) rs.getObject(1),
                (String) rs.getObject(2),
                (String) rs.getObject(3),
                (String) rs.getObject(4),
                (String) rs.getObject(5)
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Profesor al) {
        String sql;
        if (al.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?, email=?, telefono=?, especialidad=? where %s=%d", TABLE, KEY, al.getId());
        }else {
            sql = String.format("INSERT INTO %s (nombre, email, telefono, especialidad) VALUES (?,?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, al.getNombre());
            pstmt.setString(2, al.getEmail());
            pstmt.setString(3, al.getTelefono());
            pstmt.setString(4, al.getEspecialidad());
            pstmt.executeUpdate();
        if (al.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // size devuelve numero de alumnos
    public static int size() {
        return 0;
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}