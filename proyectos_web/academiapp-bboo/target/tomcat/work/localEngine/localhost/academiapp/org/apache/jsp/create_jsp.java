package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.sjava.web.*;

public final class create_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo alumno
    
        String nombre = request.getParameter("nombre");
        String email = request.getParameter("email");
        String telefono = request.getParameter("telefono");

        // aquí verificaríamos que todo ok, y solo si todo ok hacemos SAVE, 
        // por el momento lo damos por bueno...
        datosOk=true;
        if (datosOk){
            Alumno a = new Alumno(nombre, email, telefono);
            AlumnoController.save(a);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo alumno
            response.sendRedirect("list.jsp");
        }
    }



      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"es-ES\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta charset=\"utf-8\">\r\n");
      out.write("    <title>AgendApp</title>\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/estilos.css\">\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("<h1>Nuevo Alumno</h2>\r\n");
      out.write("<br>\r\n");
      out.write("<a href=\"index.jsp\">Volver</a>\r\n");
      out.write("<br>\r\n");
      out.write("<hr>\r\n");
      out.write("\r\n");
      out.write("<form action=\"#\" method=\"POST\">\r\n");
      out.write("\r\n");
      out.write("    Nombre: <input name=\"nombre\" type=\"text\">\r\n");
      out.write("    <br>\r\n");
      out.write("    Email: <input name=\"email\" type=\"text\">\r\n");
      out.write("    <br>\r\n");
      out.write("    Telefono: <input name=\"telefono\" type=\"text\">\r\n");
      out.write("    <br>\r\n");
      out.write("    <button>Crear alumno</button>\r\n");
      out.write("</form>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
