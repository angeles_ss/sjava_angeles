import java.util.TreeSet;
import java.util.Set;


class TestProvincias {
    
public static void main(String[] args) {

 Datos listado = new Datos(); // cojo los datos de la clase datos,y me devuelve un array de string
 String[] provincias= listado.provincias(); // ya tengo los datos en provincias
 Set<String> ordenadas = new TreeSet<String>(); // crea una lista con elementos ordenados

 // añado los elemento
 for (String ciudad: provincias){ // ciudad son los datos de cada linea de la lista de provincias
     ordenadas.add(ciudad);
 }
// imprimo los datos
for (String ciudad: ordenadas){
    System.out.println(ciudad);
}

}
}