package com.sjava;
import java.util.ArrayList;

public class PersonaController {

    private static ArrayList<Persona> contactos = new ArrayList<Persona>();
    private static int contador=1; // inicializo el contador a 0, que me servidar para poner un id consecutivo

    public static void muestraContactosid(int id){
      
        for (Persona p : contactos) {
            if (p.getId()==id){
                System.out.println(p);
            }
        }
    }
 
    public static void eliminarContactos(int id){ 
        Persona aborrar=null;
        for (Persona p : contactos) {
            if (p.getId()==id){
                aborrar=p; // le asigno la persona que quiero borrar y rompo el bucle
                break; 
            }
        }
        contactos.remove(aborrar); // borro el contacto que he seleccionado
    }

    public static void muestraContactos(){
        for (Persona p : contactos) {
            System.out.println(p);
        }
    }
    public static ArrayList<Persona> getContactos(){
        return contactos;
    }
    public static void nuevoContacto(Persona pers) {
        pers.setId(contador);
        contactos.add(pers);
        contador++;
    }
    public static int numContactos() {
        return contactos.size();
    }
}

