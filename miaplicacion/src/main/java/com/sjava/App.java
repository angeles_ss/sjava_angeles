package com.sjava;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        new Persona("ana", 1995, "ana@gmail.com");
        new Persona("claudia", 1999, "claudia@gmail.com");
        new Persona("maria", 1997, "maria@gmail.com");
        new Persona("julia", 1985, "julia@gmail.com");
        new Persona("lucia", 2000, "lucia@gmail.com");
        new Persona("marta", 1979, "marta@gmail.com");
        PersonaController.muestraContactos();
        System.out.println("contacto seleccionado");
        PersonaController.muestraContactosid(2);
        
        new Persona("marta", 1979, "marta@gmail.com");
        new Persona("marta", 1979, "marta@gmail.com");
        new Persona("marta", 1979, "marta@gmail.com");
        PersonaController.muestraContactos();
        PersonaController.eliminarContactos(4);
        System.out.println("contacto eliminado el id 4");
        PersonaController.muestraContactos();

       // PersonaController.muestraContactos();

}
}