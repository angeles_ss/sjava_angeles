package com.sjava;

import org.omg.CORBA.portable.Delegate;

public class Persona {

    private String nombre;
    private String email;
    private int anyo;
    private int id; // para meter un identificador a una persona

    public Persona(String nombre, int anyo, String email) {
        this.nombre = nombre;
        this.email = email;
        this.anyo = anyo;
        PersonaController.nuevoContacto(this);
    }

    public String getNombre(){
        return this.nombre;
    }

    public String getEmail(){
        return this.email;
    }
  
    public int getAnyo(){
        return this.anyo;
    }
    public int getId(){
        return this.id;
    }
    

    @Override
    public String toString() {
        return String.format("%d : %s (%s)",this.id, this.nombre, this.email); // el %d o %s es si lo que pongo hay es un numero o un texto
    }
    
    public void setId(int id){
        this.id=id;
    }
  
}
