
class Person {
    String nombre; // variables- tipo string tipo texto , si es un caracter se puede usar el char
    int edad;
    
 public Person(){}
    
    public Person(String nombre, int edad) { // constructor
        this.nombre = nombre;
        this.edad = edad;
    }


public void comparaEdad(Person otra){
    if (this.edad > otra.edad) {
        System.out.printf(
            "La %s es mayor que la %s",
            this.nombre, otra.nombre);
    } else {
        System.out.printf(
            "La %s es mayor que la %s",
            otra.nombre, this.nombre);   
    }
}
}

// las clases en Java siempre van con la primera letra en Mayuscula
// int va en minusculas porque es una primitiva