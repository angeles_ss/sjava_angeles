package com.sjava.utilidades;
import java.text.Normalizer;

public class Textos {

    public static String capitaliza(String in) {
        return in.substring(0,1).toUpperCase() + in.substring(1).toLowerCase();
    }

    public static String slug(String frase){
        String cadena = frase; //= Normalizer.normalize(frase, Form.NFC);
        cadena= cadena.toLowerCase();
        cadena=Normalizer.normalize(cadena, Normalizer.Form.NFD);  
        cadena = cadena.replaceAll("[^\\p{ASCII}]", "");
        cadena = (cadena.replace(" ", "_"));
        return cadena;
   }
}
