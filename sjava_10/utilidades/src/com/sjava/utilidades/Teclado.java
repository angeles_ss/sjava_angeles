package com.sjava.utilidades;
import java.util.Scanner;

public class Teclado{
    public static Scanner key = new Scanner(System.in);
     
    public static Int pideNumero(String msg, Int max, Int min){
        //Scanner key = new Scanner(System.in);
        Int respuesta;
        do {
            System.out.println(msg);
            respuesta= key.nextInt();
            key.nextLine();
        } while (respuesta < min || respuesta > max);
        return respuesta;
    }

    public static Int pidePalabra(String msg){
        System.out.println(msg);
        String respuesta= key.nextLine();
        return respuesta;
      
    }
  
    
}

