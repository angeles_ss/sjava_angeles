import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


class DateUtil{


    private DateUtil(){ /* para impedir que nadie cree un objeto de esta clase*/

    }
 
    public static Date createDate(int anyo, int mes, int dia){ /* devuelve un objeto tipo dato, y le tenemos que pasar 3 datos*/
        Calendar cal = Calendar.getInstance();
        cal.set(anyo, mes-1, dia); /* al mes le quito 1, porque el rango va de 0 a 11*/
        return cal.getTime(); // devuelvo el campo tipo fecha que queremos
    }

    public static Date createDateTime(int anyo, int mes, int dia, int horas, int minutos){ /* devuelve un objeto tipo dato, y le tenemos que pasar 3 datos*/
        Calendar cal = Calendar.getInstance();
        cal.set(anyo, mes-1, dia,horas, minutos); /* al mes le quito 1, porque el rango va de 0 a 11*/
        return cal.getTime(); // devuelvo el campo tipo fecha que queremos
    }

    public static  String formatDate( Date fecha, String plantilla){
        SimpleDateFormat sdf = new SimpleDateFormat(plantilla);
        return sdf.format(fecha);

    }

    public static int cuentaDomingos(int anyo, int mes){
        int domingos =0;
        int mesactual=mes-1;
        Calendar  cal= Calendar.getInstance();
        cal.set(anyo, mesactual,1);
        while (cal.get(Calendar.MONTH)==mes-1){
            if (cal.get(Calendar.DAY_OF_WEEK)==1){
                domingos++;
                System.out.println(cal.getTime());
            }
            cal.add(Calendar.DATE, 1); // suma un mes
        }
        return domingos;

       // System.out.println(cal.get(Calendar.DAY_OF_MONTH));
        //System.out.println(cal.get(Calendar.MONTH));
        //System.out.println(cal.get(Calendar.DAY_OF_WEEK));
    
        

       // return 0;

    }

}