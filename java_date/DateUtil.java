import java.util.Calendar;
import java.util.Date;


class DateUtil{


    private DateUtil(){ /* para impedir que nadie cree un objeto de esta clase*/

    }
 
    public static Date createDate(int anyo, int mes, int dia){ /* devuelve un objeto tipo dato, y le tenemos que pasar 3 datos*/
        Calendar cal = Calendar.getInstance();
        cal.set(anyo, mes-1, dia); /* al mes le quito 1, porque el rango va de 0 a 11*/
        return cal.getTime(); // devuelvo el campo tipo fecha que queremos
    }


}