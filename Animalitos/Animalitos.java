import java.util.Scanner;

class Animalitos{

public static void main(String[] args) {
    
 

Animal perro1 = new Perro();
Animal serpiente1 = new Serpiente ();
Animal pajaro1 = new Pajaro();

perro1.duerme();
serpiente1.duerme();
pajaro1.duerme();

perro1.come();
serpiente1.come();
pajaro1.come();

System.out.println("Numero de patas del perro " + perro1.getNumeroPatas() );
System.out.println("Numero de patas de la serpiente " +  serpiente1.getNumeroPatas());
System.out.println("Numero de patas del pajaro " + pajaro1.getNumeroPatas());

((Perro)perro1).ladra();
((Serpiente)serpiente1).repta();
((Pajaro)pajaro1).vuela();

Animal  perromutante1 = new PerroMutante(); // creamos una nueva varible que es Perromutante
int patas_mutante= perromutante1.getNumeroPatas(); // ejecuto contar patas, que toma el de perromutante porque sobreescribo las demas clases
System.out.println("El numero de piernas del perro mutante es  " + patas_mutante);









}   
}